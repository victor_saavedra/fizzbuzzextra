# FizzBuzz Kata

The FizzBuzz Kata requires you to produce a finite list of numbers, some of which are replaced by strings. The rules
are:

- A number divisible by three is replaced with ``'Fizz'``
- A number divisible by five is replaced with ``'Buzz'``
- A number divisible by seven is replaced with ``'Bang'``

For example:

1 2 Fizz 4 Buzz Fizz 7 8 Fizz Buzz 11 Fizz 13 14 FizzBuzz 16 17 Fizz 19 Buzz

The point is keep adding numbers' replacements and don't have to change the FizzBuzz class.